/**
 * 
 */
package com.rest.intfs.fuseapp.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author kruna
 *
 */
public class RestMappingService {

	@POST
	@Path("")
	@Consumes(MediaType.TEXT_XML)
	public Response testService(final String object) {
		return null;
	}

	@GET
	@Path("/add")
	public Response add() {
		return null;
	}
}

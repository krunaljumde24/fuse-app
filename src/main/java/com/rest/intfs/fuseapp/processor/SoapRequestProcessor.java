/**
 * 
 */
package com.rest.intfs.fuseapp.processor;

import java.util.ArrayList;
import java.util.Map;

import javax.inject.Named;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.component.cxf.common.message.CxfConstants;
import org.tempuri.Add;

/**
 * @author kruna
 *
 */
@Named("soapRequestProcessor")
public class SoapRequestProcessor implements Processor {

	@SuppressWarnings("unused")
	@Override
	public void process(Exchange exchange) throws Exception {
		exchange.getIn().setHeader(Exchange.CONTENT_TYPE, "text/xml;charset=UTF-8");
		exchange.getIn().setHeader(Exchange.ACCEPT_CONTENT_TYPE, "text/xml;charset=UTF-8");
		Map<String, Object> obj = exchange.getIn().getHeaders();
		exchange.getIn().setHeader(CxfConstants.OPERATION_NAME, "Add");
		exchange.getIn().setHeader(CxfConstants.OPERATION_NAMESPACE, "http://tempuri.org/");
		// CxfConstants.DATA_FORMAT_PROPERTY, CxfConstants.CAMEL_CXF_MESSAGE
		Add addReq = new Add();
		addReq.setIntA(10);
		addReq.setIntB(20);
		Message message = exchange.getIn();
		ArrayList<Object> reqObjList = new ArrayList<>();

		// String rawString = "Entwickeln Sie mit Vergn�gen";
		// ByteBuffer buffer1 =
		// StandardCharsets.UTF_8.encode(String.valueOf(10));
		// String utf8EncodedString1 =
		// StandardCharsets.UTF_8.decode(buffer1).toString();
		//
		// ByteBuffer buffer2 =
		// StandardCharsets.UTF_8.encode(String.valueOf(20));
		// String utf8EncodedString2 =
		// StandardCharsets.UTF_8.decode(buffer2).toString();

		reqObjList.add(10);
		reqObjList.add(20);
		exchange.getIn().setBody(reqObjList);
	}

}
